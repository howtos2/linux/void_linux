#!/usr/bin/env bash

sudo bash -c "
    xbps-install -Su &&
    xlocate -S &&
    xbps-remove -Oo &&
    vkpurge rm all
"
