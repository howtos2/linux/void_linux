## Create a custom cron-like job ##

Here we create a custom job periodically executed

### Prerequisites ###

- ```runit``` as init and service manager
- ```snooze```

### Service ###

Create the service ```my_job```.    
In this example we instruct ```snooze``` to run the job 5 minutes after midnight
even if it's 1 day late (at max), please refer to snooze(1) for the timing syntax.

```
mkdir /etc/sv/my_job
cat << EOF > /etc/sv/my_job/run
#!/bin/bash
TIMEFILE=$(dirname $0)/job.timer
exec /usr/bin/snooze -M5 -s 1d -t $TIMEFILE $(dirname $0)/dojob
EOF
```

Create the job script

```
cat << EOF > /etc/sv/my_job/dojob
#!/bin/bash
TIMEFILE=$(dirname $0)/job.timer
my_command
touch $TIMEFILE
EOF
```

### Start ###

```
chmod +x /etc/sv/my_job/run /etc/sv/my_job/dojob
ln -s /etc/sv/my_job /var/service/
```

### Check ###

```
sv status my_job
```
