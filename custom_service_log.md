## Create a custom service and custom logs ##

Here we create a custom runit service with a custom autorotated log

### Prerequisites ###

- ```runit``` as init and service manager
- ```svlogd```

### Service ###

Create the service ```my_daemon```

```
mkdir -p /etc/sv/my_daemon/log
cat << EOF > /etc/sv/my_daemon/run
#!/bin/sh
exec /path/to/my_daemond -my_option my_arguments
EOF
```

### Log ###

Create the logger

```
mkdir /var/log/my_daemon
cat << EOF > /etc/sv/my_daemon/log/run
#!/bin/sh
exec svlogd -ttt /var/log/my_daemon
EOF
```

### Start ###

```
chmod +x /etc/sv/my_daemon/run /etc/sv/my_daemon/log/run
ln -s /etc/sv/my_daemon /var/service/
```

### Check ###

```
sv status my_daemon
ls -l /var/log/my_daemon
```
