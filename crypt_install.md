## Install Void Linux Xfce desktop with full disk encryption ##

Follow the instructions here (https://docs.voidlinux.org/installation/guides/fde.html)
to have a base system to work on.    
Then login the new system and create the desktop user:

```
useradd -u 1000 -U -m -d /home/USER -s /bin/bash USER
usermod -a -G wheel,audio,video,kvm,users,storage USER
```

Install a bunch of stuff with :

```
xbps-install openssh acpi sudo xtools xfce4 xorg-server xorg-minimal xorg-video-drivers lxdm cantarell-fonts dejavu-fonts-ttf font-alias font-misc-misc font-util terminus-font firefox pulseaudio pavucontrol chrony tlp
```

If need to be ssh-reachable:

```
ln -s /etc/sv/sshd /var/service/
```

We want the old names for network interfaces, so append ``` net.ifnames=0``` to
```GRUB_CMDLINE_LINUX_DEFAULT``` in ```/etc/default/grub``` , then:

```
update-grub
```

Edit ```/etc/rc.conf``` and set:

```
TIMEZONE
HARDWARECLOCK
KEYMAP
```

And create the symlink:

```
# ZONE is the same value you put in /etc/rc.conf
ln -s /usr/share/zoneinfo/ZONE /etc/localtime
```

Check the time with the ```date``` command.

Enable dhcp client on ```eth0```:

```
ln -s /etc/sv/dhcpcd-eth0 /var/service/
```

Enable some services:

```
for i in elogind polkitd dbus lxdm uuidd chronyd tlp ; do ln -s /etc/sv/$i /var/service/ ; done
```

Reboot.

Enable non-free repository if needed (eg. for cpu microcode, https://docs.voidlinux.org/config/firmware.html):

```
xbps-install void-repo-nonfree
xbps-install -Su
```

Install the log daemon and enable it:

```
xbps-install socklog-void
ln -s /etc/sv/socklog-unix /var/service/
ln -s /etc/sv/nanoklogd /var/service/
```

Refer to https://docs.voidlinux.org/config/index.html for more.
