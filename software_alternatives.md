## Manage software alternatives in Void Linux ##

Software alternatives in Void Linux are managed by the ```xbps-alternatives(1)``` command.

### List alternatives ###

```
xbps-alternatives -l # ALL
xbps-alternatives -l PACKAGE # all alternatives provided by specific package
xbps-alternatives -l PACKAGE -g GROUP # alternatives group provided by specific package
```

### Manual set alternative ###

```
xbps-alternatives -s PACKAGE -g GROUP # set alternatives group provided by package as default
```

#### Example ####

Default ```vi``` to ```vim```:

```
xbps-alternatives -s vim-common -g vi
```
